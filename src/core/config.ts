/* eslint-disable global-require */

import { generatePageUrl } from "./utils";

export const BASE_URL = "/";
export const PRODUCTS_PER_PAGE = 6;
export const SUPPORT_EMAIL = "support@example.com";
export const PROVIDERS = {
  BRAINTREE: {
    label: "Braintree",
  },
  DUMMY: {
    label: "Dummy",
  },
  STRIPE: {
    href: "https://js.stripe.com/v3/",
    label: "Stripe",
  },
};
export const STATIC_PAGES = [
  {
    label: "About",
    url: generatePageUrl("about"),
  },
];
export const SOCIAL_MEDIA = [
  {
    ariaLabel: "facebook",
    href: "https://www.facebook.com/gethookedseafood/",
    path: require("../images/facebook-icon.svg"),
  },
  {
    ariaLabel: "instagram",
    href: "https://www.instagram.com/gethookedseafood/",
    path: require("../images/instagram-icon.svg"),
  },
  {
    ariaLabel: "youtube",
    href: "https://www.youtube.com/channel/UC18W-QIwDWeDlK7HZlGqxgA",
    path: require("../images/youtube-icon.svg"),
  },
];
export const META_DEFAULTS = {
  custom: [],
  description:
    "Get Hooked Seafood is a CSF, Community Supported Fishery, in Santa Barbara CA that provides local, fresh seafood for delivery to homes and pickup sites throughout Santa Barbara, Santa Ynez, and Ventura counties.",
  image: `${window.location.origin}${require("../images/logo.svg")}`,
  title: "Special Orders - Get Hooked Seafood",
  type: "website",
  url: window.location.origin,
};
export enum CheckoutStep {
  Address = 1,
  Shipping,
  Payment,
  Review,
}
export const CHECKOUT_STEPS = [
  {
    index: 0,
    link: "/checkout/address",
    name: "Address",
    nextActionName: "Place order",
    nextStepLink: "/order-finalized",
    onlyIfShippingRequired: true,
    step: CheckoutStep.Address,
  },
  // {
  //   index: 1,
  //   link: "/checkout/shipping",
  //   name: "Shipping",
  //   nextActionName: "Continue to Payment",
  //   nextStepLink: "/checkout/payment",
  //   onlyIfShippingRequired: true,
  //   step: CheckoutStep.Shipping,
  // },
  // {
  //   index: 2,
  //   link: "/checkout/payment",
  //   name: "Payment",
  //   nextActionName: "Continue to Review",
  //   nextStepLink: "/checkout/review",
  //   onlyIfShippingRequired: false,
  //   step: CheckoutStep.Payment,
  // },
  // {
  //   index: 1,
  //   link: "/checkout/review",
  //   name: "Review",
  //   nextActionName: "Place order",
  //   nextStepLink: "/order-finalized",
  //   onlyIfShippingRequired: false,
  //   step: CheckoutStep.Review,
  // },
];
