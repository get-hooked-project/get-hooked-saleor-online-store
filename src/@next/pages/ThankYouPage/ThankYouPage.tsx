import React from "react";
import { useHistory, useLocation } from "react-router-dom";

import { ThankYou } from "@components/organisms";
import { BASE_URL } from "@temp/core/config";
import { generateGuestOrderDetailsUrl } from "@utils/core";
import { useCheckout } from "@saleor/sdk";
import { IProps } from "./types";

const ThankYouPage: React.FC<IProps> = ({ }: IProps) => {
  const location = useLocation();
  const history = useHistory();
  console.log("location:", location);
  const { token, orderNumber } = location.state;

  // const token = "123";
  // const orderNumber = "456";
  const {
    completeCheckout
  } = useCheckout();

  const data = completeCheckout();

  console.log("thank you page data:", data);


  return (
    <ThankYou
      continueShopping={() => history.push(BASE_URL)}
      orderNumber={orderNumber}
      orderDetails={() => history.push(generateGuestOrderDetailsUrl(token))}
    />
  );
};

export { ThankYouPage };
