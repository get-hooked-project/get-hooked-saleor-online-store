import React, {
  forwardRef,
  RefForwardingComponent,
  useContext,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import { useIntl } from "react-intl";
import { RouteComponentProps, useHistory } from "react-router";

import { CheckoutAddress } from "@components/organisms";
import { useAuth, useCheckout } from "@saleor/sdk";
import { ShopContext } from "@temp/components/ShopProvider/context";
import { CHECKOUT_STEPS } from "@temp/core/config";
import { commonMessages } from "@temp/intl";
import { IAddress, ICardData, IFormError } from "@types";
import { filterNotEmptyArrayItems } from "@utils/misc";

export interface ICheckoutAddressSubpageHandles {
  submitAddress: () => void;
}

interface IProps extends RouteComponentProps<any> {
  changeSubmitProgress: (submitInProgress: boolean) => void;
}

const CheckoutAddressSubpageWithRef: RefForwardingComponent<
  ICheckoutAddressSubpageHandles,
  IProps
> = ({ changeSubmitProgress, ...props }: IProps, ref) => {
  const checkoutAddressFormId = "address-form";
  const checkoutAddressFormRef = useRef<HTMLFormElement>(null);
  const checkoutNewAddressFormId = "new-address-form";

  const history = useHistory();
  const { user } = useAuth();
  const {
    checkout,
    setShippingAddress,
    setBillingAddress,
    availableShippingMethods,
    setShippingMethod,
    selectedShippingAddressId,
    availablePaymentGateways,
    createPayment,
  } = useCheckout();
  const { countries } = useContext(ShopContext);

  const [errors, setErrors] = useState<IFormError[]>([]);

  const intl = useIntl();

  const checkoutShippingAddress = checkout?.shippingAddress
    ? {
      ...checkout?.shippingAddress,
      phone: checkout?.shippingAddress?.phone || undefined,
    }
    : undefined;

  const handleSetShippingAddress = async (
    address?: IAddress,
    email?: string,
    userAddressId?: string
  ) => {

    if (!address) {
      setErrors([
        {
          message: intl.formatMessage({
            defaultMessage: "Please provide shipping address.",
          }),
        },
      ]);
      return;
    }

    const shippingEmail = user?.email || email || "";

    if (!shippingEmail) {
      setErrors([
        {
          field: "email",
          message: intl.formatMessage(commonMessages.provideEmailAddress),
        },
      ]);
      return;
    }

    changeSubmitProgress(true);
    const { dataError } = await setShippingAddress(
      {
        ...address,
        id: userAddressId,
      },
      shippingEmail
    );

    changeSubmitProgress(false);


    const billingAddress = {} as IAddress;
    billingAddress.firstName = "cat";
    billingAddress.country = {};
    billingAddress.city = "Santa Barbara";
    billingAddress.postalCode = "91306";
    billingAddress.streetAddress1 = "street1";
    billingAddress.streetAddress2 = "street2";
    billingAddress.country.code = "US";
    billingAddress.country.country = "United States of America";
    billingAddress.countryArea = "CA";

    await setBillingAddress(
      {
        ...billingAddress,
        id: userAddressId,
      },
      "billingEmail"
    );

    const paymentGateways = await availablePaymentGateways || [];
    const shippingMethods = availableShippingMethods || [];

    console.log("dblah: " , shippingMethods);

    const data1 = await setShippingMethod(shippingMethods[0].id);
    console.log("data1.token:", data1.data.token);


    const cardData = {} as ICardData;
    cardData.brand = "VISA";
    cardData.expMonth = 1;
    cardData.expYear = 2999;
    cardData.lastDigits = "1234";
    await createPayment(paymentGateways[0].id, data1.data.token, cardData);

    // console.log("here");
    setErrors([]);
    // history.push(CHECKOUT_STEPS[0].nextStepLink);
    history.push({
      pathname: CHECKOUT_STEPS[0].nextStepLink,
      state: {
        id: data1.data.id,
        orderNumber: data1.data.number,
        token: data1.data.token,
      },
    });
  };

  const userAdresses = user?.addresses
    ?.filter(filterNotEmptyArrayItems)
    .map(address => ({
      address: {
        ...address,
        isDefaultBillingAddress: address.isDefaultBillingAddress || false,
        isDefaultShippingAddress: address.isDefaultShippingAddress || false,
        phone: address.phone || undefined,
      },
      id: address?.id || "",
      onSelect: () => null,
    }));

  useImperativeHandle(ref, () => ({
      submitAddress: async () => {
      checkoutAddressFormRef.current?.dispatchEvent(
        new Event("submit", { cancelable: true })
      );
    },
  }));

  return (
    <CheckoutAddress
      {...props}
      errors={errors}
      formId={checkoutAddressFormId}
      formRef={checkoutAddressFormRef}
      checkoutAddress={checkoutShippingAddress}
      email={checkout?.email}
      userAddresses={userAdresses}
      selectedUserAddressId={selectedShippingAddressId}
      countries={countries}
      userId={user?.id}
      newAddressFormId={checkoutNewAddressFormId}
      setShippingAddress={handleSetShippingAddress}
    />
  );
};

const CheckoutAddressSubpage = forwardRef(CheckoutAddressSubpageWithRef);

export {CheckoutAddressSubpage};
